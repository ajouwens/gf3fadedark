using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Math as Math;
using Toybox.Time as Time;
using Toybox.Time.Gregorian as Calendar;
using Toybox.WatchUi as Ui;
using Toybox.Application as App;

class gf3fadedarkView extends Ui.WatchFace {
    var hoursUi = new [5];
    var minutesUi = new [5];
    var colorsUi = new [5];
    var wdColorsUi = new [5];
    var fontsUi = new [5];
    var dayFontsUi = new [5];
    var daysPos = new [5];
    var weekDaysPos = new [5];
    var hoursPos = new [5];
    var minutesPos = new [5];
    var monthsPos = new [5];
    var battPos = new [5];
    var days = new [5];
    var weekDays = new [5];
    var months = new [5];
    var saveDay = -1;
    var saveHour = -1;
    var saveMinute = -1;
    var is24Hour = false;
    var hasNotifications = false;
    var isBTConnected = false;
    var battPerc = 101;
    var cx;
    var cy;
    var cal = [new [3],new [3],new [3],new [3],new [3]];
    var bgColor;
    var mfColor;
    var faColor;
    var hiColor;
    var coColor;
    var arColor;

    function initialize() {
        WatchFace.initialize();
        cx				 = Sys.getDeviceSettings().screenWidth/2;
        cy				 = Sys.getDeviceSettings().screenHeight/2;
        is24Hour         = Sys.getDeviceSettings().is24Hour;
        fontsUi          = [Gfx.FONT_NUMBER_MILD, Gfx.FONT_NUMBER_MEDIUM, Gfx.FONT_NUMBER_HOT, Gfx.FONT_NUMBER_MEDIUM, Gfx.FONT_NUMBER_MILD];
        dayFontsUi       = [Gfx.FONT_XTINY, Gfx.FONT_TINY, Gfx.FONT_SMALL, Gfx.FONT_TINY, Gfx.FONT_XTINY];
        if (cx == 120) {
	        daysPos          = [ [cx-35, cy-107], [cx-23, cy-95], [cx, cy-88]  , [cx+23, cy-95], [cx+35, cy-107] ];
	        weekDaysPos      = [ [cx-98, cy-50] , [cx-93, cy-28], [cx-88, cy-2], [cx-93, cy+22], [cx-98, cy+43]  ];
	        monthsPos        = [ [cx+98, cy-50] , [cx+93, cy-28], [cx+88, cy-2], [cx+93, cy+22], [cx+98, cy+43]  ];
	        battPos          = [ [cx-35, cy+107], [cx-23, cy+95], [cx, cy+88]  , [cx+23, cy+95], [cx+35, cy+107] ];
	        hoursPos         = [ [cx-59, cy-87] , [cx-31, cy-56], [cx-6, cy-1] , [cx-31, cy+50], [cx-59, cy+85]  ];
	        minutesPos       = [ [cx+59, cy-87] , [cx+31, cy-56], [cx+6, cy-1] , [cx+31, cy+50], [cx+59, cy+85]  ];
        } else {
	        daysPos          = [ [cx-30, cy-102], [cx-18, cy-90], [cx, cy-85]  , [cx+18, cy-90], [cx+30, cy-102] ];
	        weekDaysPos      = [ [cx-90, cy-45] , [cx-85, cy-23], [cx-80, cy-2], [cx-85, cy+22], [cx-90, cy+40]  ];
	        monthsPos        = [ [cx+90, cy-45] , [cx+85, cy-23], [cx+80, cy-2], [cx+85, cy+22], [cx+90, cy+40]  ];
	        battPos          = [ [cx-30, cy+102], [cx-18, cy+90], [cx, cy+85]  , [cx+18, cy+90], [cx+30, cy+102] ];
	        hoursPos         = [ [cx-52, cy-82] , [cx-24, cy-51], [cx-5, cy-4] , [cx-24, cy+45], [cx-52, cy+80]  ];
	        minutesPos       = [ [cx+52, cy-82] , [cx+24, cy-51], [cx+5, cy-4] , [cx+24, cy+45], [cx+52, cy+80]  ];
        }
        // TODO: Add notification & bluetooth icon 
        hasNotifications = Sys.getDeviceSettings().notificationCount;
        isBTConnected    = Sys.getDeviceSettings().phoneConnected;
    }

    function onLayout(dc) {
		setLayout(Rez.Layouts.WatchFace(dc));
    }

    function onShow() {
		bgColor = gf3fadedarkHelper.getColorByIndex(App.getApp().getProperty("BackgroundColor"));
        mfColor = gf3fadedarkHelper.getColorByIndex(App.getApp().getProperty("MostFadedColor"));
        faColor = gf3fadedarkHelper.getColorByIndex(App.getApp().getProperty("FadedColor"));
        hiColor = gf3fadedarkHelper.getColorByIndex(App.getApp().getProperty("HighlightedColor"));
        coColor = gf3fadedarkHelper.getColorByIndex(App.getApp().getProperty("ContrastColor"));
        arColor = gf3fadedarkHelper.getColorByIndex(App.getApp().getProperty("ArcColor"));
        colorsUi = [mfColor, faColor, hiColor, faColor, mfColor];
        wdColorsUi = [mfColor, faColor, coColor, faColor, mfColor];
    }

    function onUpdate(dc) {
        dc.setColor(bgColor, bgColor);
        dc.clear();

        var battPerc = (Sys.getSystemStats().battery).toNumber();
        var clockTime = System.getClockTime();
        var hour= clockTime.hour;
        var min = clockTime.min;
        var now = Time.now();
        var info = Calendar.info(now, Time.FORMAT_LONG);
        var batts = getBatteryValues(battPerc);
        var hourUI = gf3fadedarkHelper.getHourUI(hour, is24Hour);

        if(saveHour != hour) {
            hoursUi = getHours(hour);
            saveHour = hour;
        }

        if(saveMinute != min) {
            minutesUi = getMinutes(min);
            saveMinute = min;
        }

        if(saveDay != info.day) {
            cal = getCalendar(now);
            saveDay = info.day;
        }

		if(bgColor != arColor) {
	        dc.setColor(arColor, Gfx.COLOR_TRANSPARENT);
    	    dc.drawCircle(cx, cy-(Math.round(cy*1.15)), Math.round(cx*0.48));
        	dc.drawCircle(cx, cy+(Math.round(cy*1.15)), Math.round(cx*0.48));
	        dc.drawCircle(Math.round(-1.56 * cx) + cx, cy, cy);
    	    dc.drawCircle(Math.round(-1.03 * cx) + cx, cy, cy);
        	dc.drawCircle(Math.round(+1.03 * cx) + cx, cy, cy);
	        dc.drawCircle(Math.round(+1.56 * cx) + cx, cy, cy);
        }

        dc.setColor(hiColor, Gfx.COLOR_TRANSPARENT);
        dc.drawText(cx, cy-6, Gfx.FONT_NUMBER_HOT, ":", Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);

        for (var i = 0; i < 5; i += 1) {
            drawHourMinute(dc, hoursPos[i], colorsUi[i], fontsUi[i], hoursUi[i], "%d", Gfx.TEXT_JUSTIFY_RIGHT);
            drawHourMinute(dc, minutesPos[i], colorsUi[i], fontsUi[i], minutesUi[i], "%02d", Gfx.TEXT_JUSTIFY_LEFT);
        }

        var text = "";
        for (var i = 0; i < 5; i += 1) {
            text = cal[i][0];
            drawDay(dc, daysPos[i], wdColorsUi[i], dayFontsUi[i], Lang.format("$1$", [text]), Gfx.TEXT_JUSTIFY_CENTER);
            text = cal[i][1];
            drawDay(dc, weekDaysPos[i], wdColorsUi[i], dayFontsUi[i], Lang.format("$1$", [text]), Gfx.TEXT_JUSTIFY_CENTER);
            text = cal[i][2];
            drawDay(dc, monthsPos[i], wdColorsUi[i], dayFontsUi[i], Lang.format("$1$", [text]), Gfx.TEXT_JUSTIFY_CENTER);
            text = batts[i];
            drawDay(dc, battPos[i], wdColorsUi[i], dayFontsUi[i], Lang.format("$1$", [text]), Gfx.TEXT_JUSTIFY_CENTER);
        }

        if (!is24Hour) {
            dc.setColor(hiColor, Gfx.COLOR_TRANSPARENT);
            if (cx == 120) {
	            dc.drawText(cx-64, cy-32, Gfx.FONT_TINY, hourUI[1] , Gfx.TEXT_JUSTIFY_LEFT);
            } else {
                dc.drawText(cx-61, cy-32, Gfx.FONT_TINY, hourUI[1] , Gfx.TEXT_JUSTIFY_LEFT);
            }
        }
    }
    
    //! get day [0], weekday [1] and month [2]
    function getCalendar(now) {
    	var nn = Calendar.info(now, Time.FORMAT_SHORT);
    	var mm = nn.month;
    	for (var i = 0; i < 5; i += 1) {
			var oo = now.add(new Time.Duration((i*24-48)*3600));		
			var t1 = Calendar.info(oo, Time.FORMAT_SHORT);
			var t2 = Calendar.info(oo, Time.FORMAT_LONG);
            cal[i][0] = t1.day;
            cal[i][1] = t2.day_of_week;
        }
        
		if(cal[2][0] == 1 || cal[0][2] == null) {
	    	for (var i = 0; i < 5; i += 1) {
	            cal[i][2] = getMonth(now, mm, i-2);
	    	}
        }
        return cal;
    }

	//! now     : the current time
	//! curMonth: the current month
	//! offset  : the offset of the current month (-2 -> +2)
	function getMonth(now, curMonth, offset) {
		var reqMonth = curMonth + offset;
		if(reqMonth < 1) {
			reqMonth = curMonth + 12 + offset;
		} else if(reqMonth > 12) {
			reqMonth = curMonth - 12 + offset;
		}

		var counter = 1;
		var mm = calculateNewDate(now, offset, counter);
		var t0 = Calendar.info(mm, Time.FORMAT_SHORT);
		var t1 = Calendar.info(mm, Time.FORMAT_LONG);
		
		while(t0.month != reqMonth) {
			counter += 1;
			mm = calculateNewDate(now, offset, counter);
			t0 = Calendar.info(mm, Time.FORMAT_SHORT);
			t1 = Calendar.info(mm, Time.FORMAT_LONG);
		}
		return t1.month;
	}
	
	function calculateNewDate(now, offset, counter) {
    	var nd = counter*offset*14*24*3600;
		return now.add(new Time.Duration(nd));		
	}

    function drawHourMinute(dc, position, color, font, text, format, align) {
        dc.setColor(color, Gfx.COLOR_TRANSPARENT);
        dc.drawText(position[0], position[1], font, text.format(format), align | Gfx.TEXT_JUSTIFY_VCENTER);
    }

    function drawDay(dc, position, color, font, text, align) {
        dc.setColor(color, Gfx.COLOR_TRANSPARENT);
        dc.drawText(position[0], position[1], font, text, align | Gfx.TEXT_JUSTIFY_VCENTER);
    }

    function getHours(hour) {
        hoursUi = [hour - 2.0, hour - 1.0, hour + 0.0, hour + 1.0, hour + 2.0];
        for (var i = 0; i < hoursUi.size(); i += 1) {
            if(is24Hour && hoursUi[i] < 0) {
                hoursUi[i] += 24;
            }
            if(is24Hour && hoursUi[i] > 23) {
                hoursUi[i] -= 24;
            }
            if(!is24Hour && hoursUi[i] < 1) {
                hoursUi[i] += 12;
            }
            if(!is24Hour && hoursUi[i] > 12) {
                hoursUi[i] = hoursUi[i].toNumber() % 12;
            }
        }
        return hoursUi;
    }

    function getMinutes(minute) {
        minutesUi = [minute - 2.0, minute - 1.0, minute + 0.0, minute + 1.0, minute + 2.0];
        for (var i = 0; i < hoursUi.size(); i += 1) {
            if(minutesUi[i] < 0) {
                minutesUi[i] += 60;
            }
            if(minutesUi[i] > 59) {
                minutesUi[i] -= 60;
            }
        }
        return minutesUi;
    }

    function getBatteryValues(battPerc) {
        if (battPerc == 100) {
            return [98, 99, 100, "", ""];
        } else if (battPerc == 99) {
            return [97, 98, 99, 100, ""];
        } else if (battPerc == 1) {
            return ["", 0, 1, 2, 3];
        } else if (battPerc == 0) {
            return ["", "", 0, 1, 2];
        }
        return [battPerc - 2, battPerc -1, battPerc, battPerc + 1, battPerc + 2];
    }
}