using Toybox.System as Sys;

module gf3fadedarkHelper {
	var colorLookupTable = [
            0xFFFFFF, //ColorWhite
            0xAAAAAA, //ColorLightGray
            0x555555, //ColorDarkGray
            0x000000, //ColorBlack
            0xFF0000, //ColorRed
            0xAA0000, //ColorDarkRed
            0xFF5500, //ColorOrange
            0xAA5500, //ColorDarkOrange
            0xFFAA00, //ColorYellow
            0x00FF00, //ColorGreen
            0x00AA00, //ColorDarkGreen
            0x00AAFF, //ColorBlue
            0x0000FF, //ColorDarkBlue
            0xAA00FF, //ColorPurple
            0xFF00FF  //ColorPink
    ];
    
    function getColorByIndex(index) {
    	return colorLookupTable[ index.toNumber() ];
    }

    function getHourUI(hour, is24Hour) {
        if (!is24Hour) {
            if(hour == 0) {
                return [12, "AM"];
            } else if(hour == 12) {
                return [12, "PM"];
            } else if(hour < 12){
                return [hour.toNumber(), "AM"];
            } else {
                return [(hour.toNumber() % 12), "PM"];
            }
        }
        return [hour.toNumber(), ""];
    }
}